# Import required python libraries
import os
import time
import datetime
import sys 
import boto3
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib
# MySQL database details to which backup to be done. Make sure below user having enough privileges to take databases backup.
# To take multiple databases backup, create any file like /backup/dbnames.txt and put databses names one on each line and assignd to DB_NAME variable.
dbs=["phpmyadmin","ptc_staging","rishta_staging"]
DB_HOST = 'localhost'
DB_USER = 'root'
DB_USER_PASSWORD = ''
BACKUP_PATH = '/tmp/dbbackups'
s3 = boto3.resource('s3');
bucket = s3.Bucket('dhwani-producttion-db-backups');

# Getting current datetime to create seprate backup folder like "12012013-071334".
now = datetime.datetime.now()
today= now.strftime("%Y-%m-%d-%H:%M")
# Checking if backup folder already exists or not. If not exists will create it.
print ("creating backup folder")
if not os.path.exists(BACKUP_PATH):
  os.makedirs(BACKUP_PATH)
for db in dbs:
  print (db)
  suffix = "/" + db +"-"+ today + ".sql"
  print (suffix)
  dumpcmd = "mysqldump -u " + DB_USER + " -p" + DB_USER_PASSWORD + " " + db + " > " + BACKUP_PATH +suffix
  print ("Creating Db-backup for "+ db)
  try:
    result = os.system(dumpcmd)
    print (result)
    if (result == 0):
      backup_file = BACKUP_PATH+suffix
      zipgen = "cat"+" "+BACKUP_PATH+suffix+"|"+"gzip -9"+">"+BACKUP_PATH+suffix+".gz"
      os.system(zipgen)
      print ("DB-backup for "+ db + " ..DONE.")
      file_path = BACKUP_PATH +suffix+".gz"
      print (file_path)
      with open (file_path,'rb') as data:
        file_name = os.path.basename(file_path)
        print ('uploading ..'+' '+file_name)
        # Upload code to S3 with key and data ( key=path/with/fileName, data= actual content)
        #bucket.put_object(Key=db+'/'+today+'/'+file_name,Body=data)
        print ("Removing DB-backup file .."+ db+"-"+today+ ".sql.gz")
        os.remove(BACKUP_PATH+suffix)
        os.remove(BACKUP_PATH+suffix+".gz")
    else:
      os.remove(BACKUP_PATH+suffix)
      mail = smtplib.SMTP('smtp.gmail.com', 587)
      mail.starttls()
      mail.login("mohit.gupta@dhwaniris.com", "mohit@dhwani123")
      msg = MIMEMultipart('alternative')
      msg['Subject'] = "CRITICAL: DB BACKUP FAILED"
      msg['TO'] = "mohit.gupta@dhwaniris.com"
      body = "DB BACKUP FAILED FOR DB: "+db
      body =MIMEText(body, "plain")
      msg.attach(body)
      print (msg)
      mail.sendmail("mohit.gupta@dhwaniris.com", "mohit.gupta@dhwaniris.com", msg.as_string())
      mail.quit()

  except OSError:
      print ("EXCEPTION OCCURRED")
      pass


